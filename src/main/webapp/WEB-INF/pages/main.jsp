<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:wrapper>
    <!-- start: Content -->
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a>Dashboard</a>
            </li>
        </ul>

        <div class="row-fluid">
            <a class="quick-button metro blueDark span2" href="<c:url value="/korisnicki_panel"/>">
                <i class="icon-group"></i>
                <p>Uposlenici</p>
                <span class="badge"></span>
            </a>
            <a class="quick-button metro yellow span2" href="<c:url value="/klijenti_panel"/>">
                <i class="icon-briefcase"></i>
                <p>Klijenti</p>
                <span class="badge"></span>
            </a>
            <a class="quick-button metro greenDark span2" href="<c:url value="/certifikati"/>">
                <i class="icon-certificate"></i>
                <p>Standardi</p>
                <span class="badge"></span>
            </a>
            <a class="quick-button metro red span2" href="<c:url value="/izvjestaji_panel"/>">
                <i class="icon-file"></i>
                <p>Izvještaji</p>
                <span class="badge"></span>
            </a>
            <div class="clearfix"></div>
        </div><!--/row-->
    </div>
    <!--/.fluid-container-->
    <!-- end: Content -->
</t:wrapper>
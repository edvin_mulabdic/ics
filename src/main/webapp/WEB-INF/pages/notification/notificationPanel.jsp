<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a>Notifikacije</a>
            </li>
        </ul>
        <div class="row-fluid">
            <a class="quick-button metro greenDark span2" href="<c:url value="/threeMonthsNotifications"/>">
                <i>3</i>
                <p>Certifikati koji ističu za 3 mjeseca</p>
            </a>
            <a class="quick-button metro yellow span2" href="<c:url value="/sixMonthsNotifications"/>">
                <i>6</i>
                <p>Certifikati koji ističu za 6 mjeseci</p>
            </a>
            <a class="quick-button metro red span2" href="<c:url value="/sixMonthsNotifications"/>">
                <i>12</i>
                <p>Certifikati koji ističu za 12 mjeseci</p>
            </a>
        </div>
    </div>
</t:wrapper>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="form" uri = "http://www.springframework.org/tags/form" %>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/klijenti_panel"/>">Klijenti</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/lista_klijenata"/>">Lista klijenata</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/certifikati_za_klijenta"><c:param name="clientId" value="${certificate.client.id}"/></c:url>">Standardi klijenta</a>
                <i class="icon-angle-right"></i>
                <a>Dodaj nadzor</a>
            </li>
        </ul>
        <div class="box-content">
            <form:form method="POST" action="/createRevision" class="form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="clientId">Naziv standarda</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="clientId" name="clientId" type="text" readonly value="${certificate.certificate.certificateName}" readonly>
                    </div>
                </div>
                <div class="control-group">
                        <label class="control-label" for="certificateId">Klijent</label>
                        <div class="controls">
                            <input class="input-xlarge focused" id="certificateId" name="certificateId" type="text" readonly value="${certificate.client.clientName}" readonly>
                        </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="certificateNumber">Broj standarda</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="certificateNumber" name="certificateNumber" type="text" value="${certificate.certificateNumber}">
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="certificationDate">Datum standardizacije</label>
                    <div class="controls">
                        <input type="date"  class="date-check" name="certificationDate" id="certificationDate" value="${certificate.certificationDate}" readonly/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="revisionId">Nadzor</label>
                    <div class="controls">
                        <select  data-rel="chosen" name="revisionId" id="revisionId" required>
                                <option value="1">Prvi</option>
                                <option value="2">Drugi</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="revisionDate">Datum nadzora</label>
                    <div class="controls">
                        <input type="date"  class="date-check" name="revisionDate" id="revisionDate" required/>
                    </div>
                </div>
                <input class="input-xlarge focused" id="clientId" name="clientIdHidden" type="text" readonly value="${certificate.client.id}" style="display: none">

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Dodaj</button>
                </div>
            </form:form>
        </div>
    </div>
</t:wrapper>
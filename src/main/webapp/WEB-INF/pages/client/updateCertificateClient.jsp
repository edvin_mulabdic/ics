<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="form" uri = "http://www.springframework.org/tags/form" %>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/klijenti_panel"/>">Klijenti</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/lista_klijenata"/>">Lista klijenata</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/certifikati_za_klijenta"><c:param name="clientId" value="${certificate.client.id}"/></c:url>">Standardi klijenta</a>
                <i class="icon-angle-right"></i>
                <a>Dodaj reviziju</a>
            </li>
        </ul>
        <div class="box-content">
            <form:form method="POST" action="/updateCertificateClient" class="form-horizontal">
                <div class="control-group">
                    <label class="control-label" for="clientId">Naziv standarda</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="clientId" name="clientId" type="text" readonly value="${certificate.certificate.certificateName}" readonly>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="isAccredited">Akreditovan</label>
                    <div class="controls">
                        <select  data-rel="chosen" name="isAccredited" id="isAccredited" required>
                            <option selected value="${certificate.accredited}">
                                <c:if test="${certificate.accredited == true}">
                                    DA
                                </c:if>
                                <c:if test="${certificate.accredited == false}">
                                    NE
                                </c:if>
                            </option>
                            <option value="true">DA</option>
                            <option value="false">NE</option>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="certificateId">Klijent</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="certificateId" name="certificateId" type="text" readonly value="${certificate.client.clientName}" readonly>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="certificateNumber">Broj certifikata</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="certificateNumber" name="certificateNumber" type="text" value="${certificate.certificateNumber}" readonly>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="certificationDate">Datum standardizacije</label>
                    <div class="controls">
                        <input type="date"  class="date-check" name="certificationDate" id="certificationDate" value="${certificate.certificationDate}"/>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="standardDuration">Trajanje standarda</label>
                    <div class="controls">
                        <input class="input-xlarge focused" id="standardDuration" name="standardDuration" type="text" value="3" disabled required>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="checkbox">Promijenite trajanje standarda</label>
                    <div class="controls" style="margin-top: 15px !important;">
                        <input type="checkbox" id="checkbox" onclick="enableCertificateDurationChange()">
                    </div>
                </div>
                <input class="input-xlarge focused" id="clientId" name="clientIdHidden" type="text" readonly value="${certificate.client.id}" style="display: none">

                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Ažuriraj</button>
                </div>
            </form:form>
        </div>
    </div>

    <script>
        function enableCertificateDurationChange() {
            if($('#checkbox').is(':checked')) {
                $("#standardDuration").prop('disabled', false);
            }else {
                $("#standardDuration").prop('disabled', true);
            }
        };
    </script>

</t:wrapper>
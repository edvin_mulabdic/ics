<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix ="form" uri = "http://www.springframework.org/tags/form" %>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/klijenti_panel"/>">Klijenti</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/lista_klijenata"/>">Lista klijenata</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/certifikati_za_klijenta"><c:param name="clientId" value="${client.id}"/></c:url>">Standardi klijenta</a>
                <i class="icon-angle-right"></i>
                <a>Dodaj standard</a>
            </li>
        </ul>

        <div class="box-content">
            <form:form method="POST" action="/dodaj_certifikat_klijentu" class="form-horizontal">
                <input class="input-xlarge focused" id="clientId" name="clientId" type="text"  style="display: none" value="${client.id}">
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="name">Naziv klijenta</label>
                        <div class="controls">
                            <input class="input-xlarge focused" id="name" name="name" type="text" readonly value="${client.clientName}">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="certificateId">Naziv standarda</label>
                        <div class="controls">
                            <select  data-rel="chosen" name="certificateId" id="certificateId" required>
                                <c:forEach var="certificate" items="${certificates}">) {
                                    <option value="${certificate.id}">${certificate.certificateName}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="isAccredited">Akreditovan</label>
                        <div class="controls">
                            <select  data-rel="chosen" name="isAccredited" id="isAccredited" required>
                                <option selected value="true">DA</option>
                                <option value="false">NE</option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="certificateNumber">Broj certifikata</label>
                        <div class="controls">
                            <input class="input-xlarge focused" id="certificateNumber" name="certificateNumber" type="text" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="certificationDate">Datum standardizacije</label>
                        <div class="controls">
                            <input type="date"  class="date-check" name="certificationDate" id="certificationDate"  required/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="standardDuration">Trajanje standarda</label>
                        <div class="controls">
                            <input class="input-xlarge focused" id="standardDuration" name="standardDuration" type="text" value="3" disabled required>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="checkbox">Promijenite trajanje standarda</label>
                        <div class="controls" style="margin-top: 15px !important;">
                            <input type="checkbox" id="checkbox" onclick="enableCertificateDurationChange()">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="auditorId">Auditor</label>
                        <div class="controls">
                            <select  data-rel="chosen" name="auditorId" id="auditorId" required>
                                <c:forEach var="auditor" items="${auditori}">) {
                                    <option value="${auditor.id}">${auditor.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ea9001Id">Kod - EA 9001</label>
                        <div class="controls">
                            <select  data-rel="chosen" name="ea9001Id" id="ea9001Id">
                                <option selected>EA9001</option>
                                <c:forEach var="ea9001" items="${ea9001}">) {
                                    <option value="${ea9001.id}">${ea9001.codeNumber}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="ea14001Id">Kod - EA 14001</label>
                        <div class="controls">
                            <select  data-rel="chosen" name="ea14001Id" id="ea14001Id">
                                <option selected>EA14001</option>
                                <c:forEach var="ea14001" items="${ea14001}">) {
                                    <option value="${ea14001.id}">${ea14001.codeNumber}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-actions">
                        <button type="submit" class="btn btn-primary">Dodaj</button>
                    </div>
                </fieldset>
            </form:form>
        </div>
    </div>

    <script>
        function enableCertificateDurationChange() {
            if($('#checkbox').is(':checked')) {
                $("#standardDuration").prop('disabled', false);
            }else {
                $("#standardDuration").prop('disabled', true);
            }
        };
    </script>
</t:wrapper>
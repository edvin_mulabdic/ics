<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a>Standardi</a>
            </li>
        </ul>
        <div class="row-fluid">
            <a class="quick-button metro orange span2" href="<c:url value="/dodaj_certifikat_forma"/>">
                <i class="icon-plus"></i>
                <p>Dodaj standard</p>
            </a>
            <a class="quick-button metro blueDark span2" href="<c:url value="/lista_certifikata"/>">
                <i class="icon-list"></i>
                <p>Lista standarda</p>
            </a>
        </div>
    </div>
</t:wrapper>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a>Izvještaji</a>
            </li>
        </ul>
        <div class="row-fluid">
            <a class="quick-button metro green span2" href="<c:url value="/statistika"/>">
                <i class="icon-list"></i>
                <p>Broj klijenata</p>
            </a>
            <a class="quick-button metro greenDark span2" href="<c:url value="/klijent_standard"/>">
                <i class="icon-list"></i>
                <p>Pretraga klijenata po standardu</p>
            </a>
            <a class="quick-button metro greenLight span2" href="<c:url value="/auditori_izvjestaji"/>">
                <i class="icon-list"></i>
                <p>Auditori</p>
            </a>
        </div>
    </div>
</t:wrapper>
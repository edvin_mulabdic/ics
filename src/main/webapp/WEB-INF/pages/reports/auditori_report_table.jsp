<%--
  Created by IntelliJ IDEA.
  User: edvin.mulabdic
  Date: 12.10.2017
  Time: 19:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/izvjestaji_panel"/>">Izvještaji</a>
                <i class="icon-angle-right"></i>
                <a href="<c:url value="/auditori_izvjestaji"/>">Pretraga po auditorima</a>
                <i class="icon-angle-right"></i>
                <a>Izvještaj za auditora</a>
            </li>
        </ul>
        <div class="row-fluid sortable">
            <div class="box span12 responsive-tabela">
                <div class="box-header" data-original-title id="height">
                    <h2><i class="halflings-icon white user"></i><span class="break"></span>Izvještaj za auditora ${appUser.name}</h2>
                </div>
                <div class="box-content">
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                        <tr>
                            <th>Naziv koda 9001</th>
                            <th>Naziv koda 14001</th>
                            <th>Broj audita</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:if test="${not empty appUserStatistic}">
                            <c:forEach var="code" items="${appUserStatistic}">
                                <tr>
                                    <td>${code.ea9001.codeName}</td>
                                    <td>${code.ea14001.codeName}</td>
                                    <td>${code.counter}</td>
                                </tr>
                            </c:forEach>
                        </c:if>
                        </tbody>
                    </table>
                </div>
            </div><!--/span-->
        </div><!--/row-->
    </div>
</t:wrapper>

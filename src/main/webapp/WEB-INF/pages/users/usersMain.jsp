<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<t:wrapper>
    <div id="content" class="span10">
        <ul class="breadcrumb">
            <li>
                <i class="icon-dashboard"></i>
                <a href="<c:url value="/main"/>">Dashboard</a>
                <i class="icon-angle-right"></i>
                <a>Uposlenici</a>
            </li>
        </ul>
        <div class="row-fluid">
            <a class="quick-button metro greenDark span2" href="<c:url value="/dodaj_korisnika_forma"/>">
                <i class="icon-plus"></i>
                <p>Dodaj uposlenika</p>
            </a>
            <a class="quick-button metro yellow span2" href="<c:url value="/lista_korisnika"/>">
                <i class="icon-list"></i>
                <p>Lista uposlenika</p>
            </a>
        </div>
    </div>
</t:wrapper>
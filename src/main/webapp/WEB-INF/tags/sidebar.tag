<%@tag description="Sidebar" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- start: Main Menu -->
<div id="sidebar-left" class="span2">
    <div class="nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li><a href="<c:url value="/main"/>"><i class="icon-dashboard"></i><span
                    class=" hidden-tablet">
                            Dashboard</span></a></li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-group"></i><span class="hidden-tablet">
                                Uposlenici</span> <span
                        class="label label-important"></span></a>
                <ul>
                    <li><a class="submenu" href="<c:url value="/korisnicki_panel"/>"><i
                            class="icon-bar-chart"></i><span class="hidden-tablet">
                                    Uposlenici panel</span></a></li>
                    <li><a class="submenu" href="/dodaj_korisnika_forma"><i
                            class="icon-plus"></i><span class="hidden-tablet">
                                    Dodaj uposlenika</span></a></li>
                    <li><a class="submenu" href="<c:url value="/lista_korisnika"/>"><i
                            class="icon-list"></i><span class="hidden-tablet">
                                    Lista uposlenika</span></a></li>
                    <li><a class="submenu" href="<c:url value="/ea9001_tabela"/>"><i
                        class="icon-list"></i><span class="hidden-tablet">
                                    EA-9001 Tabela</span></a></li>
                    <li><a class="submenu" href="<c:url value="/ea14001_tabela"/>"><i
                            class="icon-list"></i><span class="hidden-tablet">
                                    EA-14001 Tabela</span></a></li>
                </ul>
            </li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-briefcase"></i><span class="hidden-tablet">
                                Klijenti</span> <span
                        class="label label-important"></span></a>
                <ul>
                    <li><a class="submenu" href="<c:url value="/klijenti_panel"/>"><i class="icon-bar-chart"></i><span
                            class="hidden-tablet">
                                    Klijenti panel </span></a></li>
                    <li><a class="submenu" href="<c:url value="/dodaj_klijenta_forma"/>"><i class="icon-plus"></i><span
                            class="hidden-tablet">
                                    Dodaj klijenta </span></a></li>
                    <li><a class="submenu" href="<c:url value="/lista_klijenata"/>"><i class="icon-list"></i><span
                            class="hidden-tablet">
                                    Lista klijenata</span></a></li>
                </ul>
            </li>

            <li>
                <a class="dropmenu" href="#"><i class="icon-certificate"></i><span class="hidden-tablet">
                                Standardi</span> </a>
                <ul>
                    <li><a class="submenu" href="<c:url value="/certifikati"/>"><i
                            class="icon-bar-chart"></i><span class="hidden-tablet">
                                    Standardi panel</span></a></li>
                    <li><a class="submenu" href="<c:url value="/dodaj_certifikat_forma"/>"><i class="icon-plus"></i><span
                            class="hidden-tablet">
                                    Dodaj standard</span></a></li>
                    <li><a class="submenu" href="<c:url value="/lista_certifikata"/>"><i class="icon-list"></i><span
                            class="hidden-tablet">
                                    Lista standarda</span></a></li>
                </ul>
            </li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-file"></i><span class="hidden-tablet">
                                EA Kodovi</span> </a>
                <ul>
                    <li><a class="submenu" href="<c:url value="/eaCodes"/>"><i
                            class="icon-bar-chart"></i><span class="hidden-tablet">
                                    EA kodovi panel</span></a></li>
                </ul>
            </li>
            <li>
                <a class="dropmenu" href="#"><i class="icon-file"></i><span class="hidden-tablet">
                                Izvještaji</span> </a>
                <ul>
                    <li><a class="submenu" href="<c:url value="/izvjestaji_panel"/>"><i
                            class="icon-bar-chart"></i><span class="hidden-tablet">
                                    Izvještaji panel</span></a></li>

                </ul>
            </li>
        </ul>
    </div>
    <input id="numOfMails" value="" hidden>
</div>
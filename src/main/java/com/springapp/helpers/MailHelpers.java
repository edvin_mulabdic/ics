package com.springapp.helpers;

/**
 * Created by Edvin Mulabdic
 */
public class MailHelpers {
    public static final String MAIL_FROM = "ics.informacije@gmail.com";
    public static final String MAIL_SUBJECT = "Obavijest o isteku certifikata";
    public static final String MAIL_TO = "edvin.mulabdic@gmail.com";

}

package com.springapp.helpers;

/**
 * Created by Edvin Mulabdic
 */
public class MessageHelpers {
    public static final String MAIL_SUCCESS_MSG = "Vaša poruka je uspješno poslana.";
    public static final String MAIL_ERROR_MSG = "Došlo je do greške.Vaša poruka nije poslana.";
}

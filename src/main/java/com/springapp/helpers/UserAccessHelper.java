package com.springapp.helpers;

import org.springframework.security.crypto.bcrypt.BCrypt;

/**
 * Created by Edvin Mulabdic
 */
public class UserAccessHelper {

    public static final String ADMIN = "Admin";
    public static final String AUDITOR = "Auditor";
    public static final String TEHNICKI_EXPERT = "Tehnicki ekspert";
    public static final String DIREKTOR = "Direktor";
    public static final String OPERATION_PLANNER = "Operation planner";
    public static final String LEAD_AUDITOR = "Lead auditor";
    public static final String SSCOM = "SSCOM";
    public static final String LICE_NA_OBUCI = "Lice na obuci";
    public static final String UNAUTHORIZED = "unauthorized";

    public String adminAccess() {
        return "Admin";
    }

    public String userAccess() {
        return "user";
    }

    public String unauthorized() {
        return "unauthorized";
    }
}

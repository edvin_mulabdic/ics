package com.springapp.controller;

import com.springapp.model.AppUser;
import com.springapp.service.AppUserService;
import com.springapp.service.EA14001Service;
import com.springapp.service.EA9001Service;
import com.springapp.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by ajla.eltabari on 29.5.2017.
 */
@Controller
public class AppUserController {
    @Autowired
    AppUserService appUserService;
    @Autowired
    EA9001Service ea9001Service;
    @Autowired
    EA14001Service ea14001Service;
    @Autowired
    RoleService roleService;

    @RequestMapping(value= "/korisnicki_panel", method = RequestMethod.GET)
    public String personsMain() {
        return "users/usersMain";
    }

    @RequestMapping(value = "/dodaj_korisnika_forma", method = RequestMethod.GET)
    public String createAppUserRender(Model model) {
        model.addAttribute("ea9001", ea9001Service.getAllEACodes());
        model.addAttribute("ea14001", ea14001Service.getAllEACodes());
        model.addAttribute("userAccess", roleService.getAllRoles());
        return "users/createUser";
    }

    @RequestMapping(value = "/dodaj_korisnika", method = RequestMethod.POST)
    public String createUser(@RequestParam("name") String name, @RequestParam("address") String address,
                             @RequestParam("city") String city, @RequestParam("email") String email, @RequestParam("phone") String phone,
                             @RequestParam("qualifications") String qualifications, @RequestParam("role") String role,
                             @RequestParam("password") String password, @RequestParam("ea9001") String ea9001, @RequestParam("ea14001") String ea14001,
                             Model model) {
        String[] ea9001Id = new String[0];
        String[] ea14001Id = new String[0];
        if(ea9001 != "") {
            ea9001Id = ea9001.split(",");
        }
        if(ea14001 != "") {
            ea14001Id = ea14001.split(",");
        }
        if (appUserService.createUser(name, address, city, email, phone, qualifications,role, password, ea9001Id, ea14001Id)) {
            model.addAttribute("success", "Korisnik je uspješno dodan.");
            return "redirect:/lista_korisnika";
        } else {
            model.addAttribute("error", "Neuspješno dodavanje korisnika!");
            return "redirect:/dodaj_korisnika_forma";
        }
    }

    @RequestMapping(value = "/brisanje_korisnika", params = "userId", method = RequestMethod.GET)
    public String deleteAppUser(@RequestParam("userId") String userId, Model model) {
        if (appUserService.deleteUser(Integer.parseInt(userId))) {
            model.addAttribute("success", "Korisnik uspješno obrisan." );
        } else {
            model.addAttribute("error", "Brisanje korisnika nije uspjelo!");
        }
        return "redirect:/lista_korisnika";
    }

    @RequestMapping(value = "/update_korisnika_forma", params ={"appUserId"}, method = RequestMethod.GET)
    public String updateAppUserRender(@RequestParam("appUserId") String appUserId, Model model) {
        AppUser appUser = appUserService.getUserById(appUserId);
        model.addAttribute("user", appUser);
        model.addAttribute("ea9001", ea9001Service.getAllEACodes());
        model.addAttribute("ea14001", ea14001Service.getAllEACodes());
        model.addAttribute("userAccess", roleService.getAllRoles());
        return "users/updateUser";
    }

    @RequestMapping(value= "/update_korisnika", method = RequestMethod.POST)
    public String updateCertificate(@RequestParam("name") String name, @RequestParam("address") String address,
                                    @RequestParam("city") String city, @RequestParam("email") String email, @RequestParam("phone") String phone,
                                    @RequestParam("qualifications") String qualifications, @RequestParam("role") String role,
                                    @RequestParam("password") String password, HttpServletRequest request,  @RequestParam("ea9001") String ea9001, @RequestParam("ea14001") String ea14001,
                                    Model model) {
        String[] ea9001Id = new String[0];
        String[] ea14001Id = new String[0];
        if(ea9001 != "") {
            ea9001Id = ea9001.split(",");
        }
        if(ea14001 != "") {
            ea14001Id = ea14001.split(",");
        }
        if (appUserService.updateUser(name, address, city, email, phone, qualifications, role, password, ea9001Id, ea14001Id, request.getParameter("appUserId"))) {
            model.addAttribute("success", "Korisnik je uspješno ažuriran.");
        } else {
            model.addAttribute("error", "Korisnik nije ažuriran.");
        }
        return "redirect:/lista_korisnika";
    }

    @RequestMapping(value = "/lista_korisnika", method = RequestMethod.GET)
    public String listOfPersonsRender(@RequestParam(value = "success", required = false) String success,
                                      @RequestParam(value = "error", required = false) String error, Model model) {
        model.addAttribute("users", appUserService.getAllUsers());
        model.addAttribute("success", success);
        model.addAttribute("error", error);
        return "users/listOfUsers";
    }

    @RequestMapping(value = "/ea9001_tabela", method = RequestMethod.GET)
    public String ea9001Table(Model model) {
        model.addAttribute("ea9001", ea9001Service.getAllEACodes());
        model.addAttribute("appUsers", appUserService.getAllUsers());
        return "ea_schema/ea9001";
    }

    @RequestMapping(value = "/ea14001_tabela", method = RequestMethod.GET)
    public String ea14001Table(Model model) {
        model.addAttribute("ea14001", ea14001Service.getAllEACodes());
        model.addAttribute("appUsers", appUserService.getAllUsers());
        return "ea_schema/ea14001";
    }
}

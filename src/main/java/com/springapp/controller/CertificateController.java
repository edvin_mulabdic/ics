package com.springapp.controller;

import com.springapp.model.Certificate;
import com.springapp.service.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by edvin.mulabdic
 */
@Controller
public class CertificateController {
    @Autowired
    CertificateService certificateService;

    @RequestMapping(value = "/certifikati", method = RequestMethod.GET)
    public String certificates() {
        return "certificates/certificateMain";
    }

    @RequestMapping(value= "/dodaj_certifikat_forma", method = RequestMethod.GET)
    public String createCertificateRender() {
        return "certificates/createCertificate";
    }

    @RequestMapping(value= "/dodaj_certifikat", method = RequestMethod.POST)
    public String createCertificate(@RequestParam("certificateName") String certificateName,
                                    @RequestParam("certificateDuration") String certificateDuration, Model model) {
        if(certificateService.createCertificate(certificateName,certificateDuration)) {
            model.addAttribute("success", "Certifikat uspješno dodan.");
        } else {
            model.addAttribute("error", "Certifikat nije dodan.");
        }
        return "redirect:/lista_certifikata";
    }

    @RequestMapping(value= "/update_certifikat_forma", params ={"certificateId"}, method = RequestMethod.GET)
    public String updateCertificateRender(@RequestParam("certificateId") String certificateId, Model model) {
        Certificate certificate = certificateService.getCertificateById(Integer.parseInt(certificateId));
        model.addAttribute("certificate", certificate);
        return "certificates/updateCertificate";
    }

    @RequestMapping(value= "/update_certifikat", method = RequestMethod.POST)
    public String updateCertificate(@RequestParam("certificateName") String certificateName,
                                    @RequestParam("certificateDuration") String certificateDuration, HttpServletRequest request, Model model) {
        if(certificateService.updateCertificate(certificateName, certificateDuration, request.getParameter("certificateId"))) {
            model.addAttribute("success", "Certifikati uspješno ažuriran.");
        } else {
            model.addAttribute("error", "Certifikat nije ažuriran.");
        }
        return "redirect:/lista_certifikata";
    }

    @RequestMapping(value= "/lista_certifikata", method = RequestMethod.GET)
    public String listOfCertificates(@RequestParam(value = "success", required = false) String success,
                                     @RequestParam(value = "error", required = false) String error, Model model) {
        model.addAttribute("certificates", certificateService.getAllCertificates());
        model.addAttribute("success", success);
        return "certificates/listOfCertificates";
    }

    @RequestMapping(value= "/brisanje_certifikata",params = "certificateId", method = RequestMethod.GET)
    public String deleteCertificate(@RequestParam(value = "certificateId") String certificateId, Model model) {
        if(certificateService.deleteCertificate(certificateId)) {
            model.addAttribute("success", "Certifikat uspješno obrisan.");
        } else {
            model.addAttribute("error", "Certifikat nije obrisan.");
        }
        return "redirect:/lista_certifikata";
    }


}

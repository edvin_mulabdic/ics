package com.springapp.controller;

import com.springapp.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.transaction.Transactional;

/**
 * Created by edvin.mulabdic
 */
@Controller
@Transactional
public class NotificationController {
    @Autowired
    NotificationService notificationService;

    @RequestMapping(value="/", method = RequestMethod.POST)
    public void sendEmails() {
        notificationService.sendEmails();
    }

    @RequestMapping(value="/notifikacije_panel", method = RequestMethod.GET)
    public String notificationPanel(Model model) {
        model.addAttribute("oneYear", notificationService.sendEmail1Year());
        model.addAttribute("sixMonths", notificationService.sendEmail6Months());
        model.addAttribute("threeMonths", notificationService.sendEmail3Months());
        return "notification/notificationPanel";
    }


    @RequestMapping(value="/oneYearNotifications", method = RequestMethod.GET)
    public String oneYearNotifications(Model model) {
        model.addAttribute("oneYear", notificationService.sendEmail1Year());
        model.addAttribute("oneYearMessage", "Certifikat ističe za 1 godinu");
       return "notification/oneYearNotifications";
    }

    @RequestMapping(value="/sixMonthsNotifications", method = RequestMethod.GET)
    public String sixMonthsNotifications(Model model) {
        model.addAttribute("sixMonths", notificationService.sendEmail6Months());
        model.addAttribute("sixMonthsMessage", "Certifikat ističe za 6 mjeseci");
        return "notification/sixMonthsNotifications";
    }

    @RequestMapping(value="/threeMonthsNotifications", method = RequestMethod.GET)
    public String threeMonthsNotifications(Model model) {
        model.addAttribute("threeMonths", notificationService.sendEmail3Months());
        model.addAttribute("threeMonthsMessage", "Certifikat ističe za 3 mjeseca");
        return "notification/threeMonthsNotifications";
    }

}

package com.springapp.controller;

import com.springapp.helpers.ClientHelper;
import com.springapp.model.Client;
import com.springapp.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.text.ParseException;

/**
 * Created by ${Edvin-Mulabdic} on 5/21/2017.
 */
@Controller
public class ClientController {
    @Autowired
    ClientService clientService;

    @RequestMapping(value= "/klijenti_panel", method = RequestMethod.GET)
    public String personsMain() {
        return "client/clientMain";
    }

    @RequestMapping(value = "/dodaj_klijenta_forma", method = RequestMethod.GET)
    public String createPersonsRender() {
        return "client/createClient";
    }

    @RequestMapping(value = "/dodaj_klijenta", method = RequestMethod.POST)
    public String createPerson(@RequestParam("clientName") String clientName, @RequestParam("contactPerson") String contactPerson,
                               @RequestParam("positionInOrganization") String positionInOrganization, @RequestParam("address") String address,
                               @RequestParam("address2") String address2, @RequestParam("phone") String phone, @RequestParam("email") String email,
                               @RequestParam("webAddress") String webAddress, @RequestParam("clientPDVNumber") String clientPDVNumber,
                                HttpServletRequest request, Model model) {

        String isTransferred = request.getParameter("isTransferred");
        Boolean prelaznik = false;
        if(isTransferred != null) {
            prelaznik = isTransferred.equals("on");
        }
        if(clientService.createClient(clientName, contactPerson, positionInOrganization, address, address2, phone, email, webAddress, clientPDVNumber, prelaznik)) {
            model.addAttribute("success", "Klijent je uspješno dodan.");
            return "redirect:/lista_klijenata";
        } else {
            model.addAttribute("error", "Neuspješno dodavanje klijenta!");
            return "redirect:/dodaj_klijenta_forma";
        }
    }

    @RequestMapping(value = "/lista_klijenata", method = RequestMethod.GET)
    public String listOfPersonsRender(@RequestParam(value = "success", required = false) String success,
                                      @RequestParam(value = "error", required = false) String error, Model model) {
        model.addAttribute("clients", clientService.getAllClients());
        model.addAttribute("success", success);
        model.addAttribute("error", error);
        return "client/listOfClients";
    }

    @RequestMapping(value = "/update_klijent_forma", params ={"clientId"} ,method = RequestMethod.GET)
    public String updateClientRender(@RequestParam("clientId") String clientId, Model model) {
        Integer clientIdd = Integer.valueOf(clientId);
        Client client = clientService.findClientById(clientIdd);
        model.addAttribute("client", client);
        return "client/updateClient";
    }

    @RequestMapping(value = "/update_klijenta", method = RequestMethod.POST)
    public String  updateClient(@RequestParam("clientName") String clientName, @RequestParam("contactPerson") String contactPerson,
                                @RequestParam("positionInOrganization") String positionInOrganization, @RequestParam("address") String address,
                                @RequestParam("address2") String address2, @RequestParam("phone") String phone, @RequestParam("email") String email,
                                @RequestParam("webAddress") String webAddress, @RequestParam("clientPDVNumber") String clientPDVNumber,
                                @RequestParam("clientStatus") ClientHelper.clientStatus clientStatus, HttpServletRequest request, Model model)
                throws ParseException, IOException {

        Integer clientId = Integer.parseInt(request.getParameter("clientId"));

        if(clientService.updateClient(clientName, contactPerson, positionInOrganization, address, address2, phone, email, webAddress, clientPDVNumber, clientStatus, clientId)) {
            model.addAttribute("success", "Klijent uspješno ažuriran.");
        } else {
            model.addAttribute("error", "Ažuriranje klijenta nije uspjelo!");
        }
        return "redirect:/lista_klijenata";
    }

    @RequestMapping(value = "/brisanje_klijenta",params = "clientId", method = RequestMethod.GET)
    public String deleteClient(@RequestParam("clientId") String clientId, Model model) {
        if(clientService.deleteClient(Integer.parseInt(clientId))) {
            model.addAttribute("success", "Klijent uspješno obrisan." );
        } else {
            model.addAttribute("error", "Brisanje klijenta nije uspjelo!");
        }
        return "redirect:/lista_klijenata";
    }

}

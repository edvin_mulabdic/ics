package com.springapp.controller;

import com.springapp.model.Certificate;
import com.springapp.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by edvin.mulabdic
 */

@Controller
public class ReportsController {
    @Autowired
    ClientService clientService;
    @Autowired
    CertificateService certificateService;
    @Autowired
    CertificateClientService certificateClientService;
    @Autowired
    AppUserService appUserService;
    @Autowired
    EACodeAppUserService eaCodeAppUserService;

    @RequestMapping(value= "/izvjestaji_panel", method = RequestMethod.GET)
    public String reportsPanel() {
        return "reports/reportsPanel";
    }

    @RequestMapping(value= "/statistika", method = RequestMethod.GET)
    public String statistic(Model model) {
        model.addAttribute("datum", new Date());
        model.addAttribute("brojKlijenata", clientService.getAllClients().size());
        return "reports/statistic";
    }

    @RequestMapping(value= "/klijent_standard", method = RequestMethod.GET)
    public String clientStandard(Model model) {
        model.addAttribute("standards", certificateService.getAllCertificates());
        return "reports/clients_standard";
    }

    @RequestMapping(value= "/klijent_standard_tabela", method = RequestMethod.GET)
    public String clientStandardTable(@RequestParam("standard") String standard,
                                      @RequestParam("isAccredited") String isAccredited,
                                      Model model) {
        Boolean accredited = true;
        if(("isAccredited").equals("true")) {
            accredited = true;
        }else if (("isAccredited").equals("false"))  {
            accredited = false;
        }
        model.addAttribute("clients", certificateClientService.getClientsByCertificateId(Integer.parseInt(standard), accredited));
        model.addAttribute("standard", certificateService.getCertificateById(Integer.parseInt(standard)));
        return "reports/clients_standard_table";
    }

    @RequestMapping(value= "/auditori_izvjestaji", method = RequestMethod.GET)
    public String auditorReports(Model model) {
        model.addAttribute("auditori", appUserService.getAllAuditUsers());
        return "reports/auditori_report_panel";
    }

    @RequestMapping(value= "/auditori_report_table", method = RequestMethod.GET)
    public String auditorReportTable(@RequestParam("auditor") String appUserId, Model model) {
        model.addAttribute("appUserStatistic", eaCodeAppUserService.getAllEACodesForAppUserStatistic(appUserId));
        model.addAttribute("appUser", appUserService.getUserById(appUserId));
        return "reports/auditori_report_table";
    }

}

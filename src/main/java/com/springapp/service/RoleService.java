package com.springapp.service;

import com.springapp.model.Role;

import java.util.List;

/**
 * Created by edvin.mulabdic
 */
public interface RoleService {
    List<Role> getAllRoles();
}

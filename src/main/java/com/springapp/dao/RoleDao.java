package com.springapp.dao;

import com.springapp.model.Role;

import java.util.List;

/**
 * Created by edvin.mulabdic
 */
public interface RoleDao {
    Role getUserAccessRole(String role);

    List<Role> getAllRoles();
}
